# {{project-name}}

[![Gitlab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/{{gitlab-namespace}}%2F{{gitlab-project}}?style=flat-square)](https://gitlab.com/{{gitlab-namespace}}/{{gitlab-project}}/-/pipelines)
[![GitLab Release](https://img.shields.io/gitlab/v/release/{{gitlab-namespace}}%2F{{gitlab-project}}?style=flat-square)](https://gitlab.com/{{gitlab-namespace}}/{{gitlab-project}}/-/releases)
[![Static Badge](https://img.shields.io/badge/license-GPL%203.0-blue?style=flat-square)](https://gitlab.com/{{gitlab-namespace}}/{{gitlab-project}}/-/blob/main/LICENSE)
[![Static Badge](https://img.shields.io/badge/Open%20Source%3F-Yes!-blue?style=flat-square)](#)

> {{project-description}}

**Main features**
 - 

## Installation (Ubuntu Jammy 22.04)

```bash
sudo sh -c 'curl -SsL https://sgtools.gitlab.io/ppa/pgp-key.public | gpg --dearmor > /etc/apt/trusted.gpg.d/sgtools.gpg'
sudo sh -c 'curl -SsL -o /etc/apt/sources.list.d/sgtools.list https://sgtools.gitlab.io/ppa/sgtools.list'
sudo apt update
sudo apt install {{crate_name}}
```

## Usage

```bash
{{crate_name}} --help
```

See [documentation page](https://{{gitlab-namespace}}.gitlab.io/{{gitlab-project}})

## Dependencies

- 

## License

Copyright (C) {{year}} Sebastien Guerri

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

## Contributors

- Sébastien Guerri: [@sguerri](https://gitlab.com/sguerri)
