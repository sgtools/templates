crate := "{{gitlab-namespace}}/{{gitlab-project}}"

git_host := "gitlab.com"
user_name := "Sébastien GUERRI"
user_email := "5775784-sguerri@users.noreply.gitlab.com"

version := `cargo metadata --no-deps --format-version 1 | jq -r .packages[0].version`

{% raw %}

##################################################################################################
## SPECIFIC  #####################################################################################
##################################################################################################

# Main: Show project infos
[group("main")]
@infos:
    bat Cargo.toml --color always

# Main: Build
[group("main")]
@build:
    cargo build

# Main: Run
[group("main")]
@run:
    cargo run

# Main: Test
[group("main")]
@test:
    cargo test

# Version: Bump version
[group("version")]
@version_bump v:
    sed -i "0,/version = \"{{version}}\"/{s//version = \"{{v}}\"/}" "./Cargo.toml"
    sed -i "0,/PACKAGE_VERSION: \"{{version}}\"/{s//PACKAGE_VERSION: \"{{v}}\"/}" "./.gitlab-ci.yml"

# Dependencies: Check for available updated
[group("dependencies")]
@deps_check:
    cargo upgrade --dry-run

# Dependencies: Update
[group("dependencies")]
@deps_update:
    cargo upgrade && cargo update

# Dependencies: Check unused
[group("dependencies")]
@deps_unused:
    cargo machete .

# Doc: Create documentation
[group("doc")]
@create_doc:
    cargo doc --no-deps --release


##################################################################################################
## GENERIC  ######################################################################################
##################################################################################################

version_M := shell("echo $1 | cut -d'.' -f 1", version)
version_m := shell("echo $1 | cut -d'.' -f 2", version)
version_P := shell("echo $1 | cut -d'.' -f 3", version)
version_next_major := shell("echo $1 + 1 | bc", version_M) + ".0.0"
version_next_minor := version_M + "." + shell("echo $1 + 1 | bc", version_m) + ".0"
version_next_patch := version_M + "." + version_m + "." + shell("echo $1 + 1 | bc", version_P)

alias patch := release_patch
alias minor := release_minor
alias major := release_major

# Release: New patch
[group("release")]
@release_patch: (release version_next_patch)

# Release: New minor
[group("release")]
@release_minor: (release version_next_minor)

# Release: New major
[group("release")]
@release_major: (release version_next_major)

# Release
[group("release")]
@release v: (version_bump v) build (create_changelog v) (git_release v)

# Git: Ui
[group("git")]
@git_ui:
    gitui

# Git: Init repository
[group("git")]
[no-exit-message]
@git_init:
    if [ ! -f "README.md" ]; then echo "File README.md does not exist"; exit 1; fi
    if [ ! -f "CHANGELOG.md" ]; then echo "File CHANGELOG.md does not exist"; exit 1; fi
    if [ ! -f "LICENSE" ]; then echo "File LICENSE does not exist"; exit 1; fi
    if [ ! -f ".gitignore" ]; then echo "File .gitignore does not exist"; exit 1; fi
    git init --initial-branch=main
    git config user.name {{user_name}}
    git config user.email {{user_email}}
    git remote add origin git@{{git_host}}:{{crate}}.git
    git add README.md CHANGELOG.md LICENSE .gitignore
    git commit -m "Initial commit"
    git push -u origin main
    git checkout -b develop
    git add .
    git commit -m "Initial commit"
    git push -u origin develop

# Git: Create a new release
[group("git")]
@git_release v:
    git checkout -b release-{{v}} develop
    git commit -a -m "Bumped version number to {{v}}"
    git checkout develop
    git merge --no-edit --no-ff release-{{v}}
    git push origin develop
    git checkout main
    git merge --no-edit --no-ff develop -m "Version {{v}}"
    git tag -a "v{{v}}" -m "Version {{v}}"
    git push origin main
    git branch -d release-{{v}}
    git checkout develop

# Version: Get current version
[group("version")]
@version_get:
    echo {{version}}

# Doc: Create changelog
[group("doc")]
@create_changelog v:
    git cliff --unreleased --tag v{{v}} --sort newest -c .cliff.toml --prepend CHANGELOG.md

{% endraw %}

