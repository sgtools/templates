// {{project-name}} - {{project-description}}
// Copyright (C) {{year}} Sebastien Guerri
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use anyhow::Result;
use sgtools_libs as utils;

mod cli;
mod commands;
mod config;

use config::*;

fn main() -> Result<()>
{
    let matches = cli::build_cli().get_matches();
    let context = utils::param::get_str(&matches, "context", "DEFAULT")?;
    let context = utils::config::get_or_init::<Context, _, _>(clap::crate_name!(), context)?;

    match matches.subcommand() {
        Some(("default", args)) => commands::default::run(args, &context)?,
        Some((_, _)) => unreachable!(),
        None => commands::default::run(&matches, &context)?,
    };

    Ok(())
}
