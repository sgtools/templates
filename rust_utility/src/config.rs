// {{project-name}} - {{project-description}}
// Copyright (C) {{year}} Sebastien Guerri
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use anyhow::Result;
use serde::Deserialize;
use serde::Serialize;

use crate::utils::config::*;

// SPECIFIC
#[derive(Debug, Default, Deserialize, Serialize, Clone)]
pub struct Context
{
    pub name: String,
}

// SPECIFIC
impl ContextTrait for Context
{
    fn get_name(&self) -> String
    {
        self.name
            .clone()
    }

    fn init<S: AsRef<str>>(&mut self, name: S) -> Result<()>
    {
        self.name = name
            .as_ref()
            .to_string();
        Ok(())
    }
}
