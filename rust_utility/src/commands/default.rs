// {{project-name}} - {{project-description}}
// Copyright (C) {{year}} Sebastien Guerri
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use anyhow::Result;
use clap::ArgMatches;

use crate::Context;

/// Run command
pub fn run(args: &ArgMatches, context: &Context) -> Result<()>
{
    println!("{:#?}", context);

    let json = crate::utils::param::get_bool(args, "json");
    let no_tty = crate::utils::param::get_bool(args, "no-tty");
    let verbose = crate::utils::param::get_bool(args, "verbose");
    let color = crate::utils::param::get_str(args, "color", "auto")?;

    println!("JSON    : {}", json);
    println!("NO_TTY  : {}", no_tty);
    println!("VERBOSE : {}", verbose);
    println!("COLOR   : {}", color);

    Ok(())
}
