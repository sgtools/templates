// {{project-name}} - {{project-description}}
// Copyright (C) {{year}} Sebastien Guerri
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use clap::arg;
use clap::Command;

/// Creates CLI clap [Command][1]
///
/// [1]: https://docs.rs/clap/latest/clap/struct.Command.html
pub fn build_cli() -> Command
{
    Command::new(clap::crate_name!())
        .about(clap::crate_description!())
        .version(clap::crate_version!())
        .subcommand_required(false)
        .infer_subcommands(true)
        .allow_external_subcommands(false)
        .arg(arg!(-c --context <CONTEXT> "Set application context"))
        .arg(arg!(-v --verbose "Turns on verbose output").global(true))
        .arg(arg!(-n --"no-tty" "Turns on piped input").global(true))
        .arg(arg!(-j --json "Turns on json output").global(true))
        .arg(
            arg!(--color <COLOR> "Coloring: auto always never")
                .value_parser(["auto", "always", "never"])
                .default_value("auto")
                .global(true),
        )
        .subcommand(Command::new("default").about("This is the default command"))
}
